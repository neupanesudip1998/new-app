const express = require("express");

const router = express.Router();

const {
  getUser,
  getUsers,
  createUser,
  removeUser,
  updateUser,
} = require("../controllers/admin");

const {
  getPost,
  getPosts,
  createPost,
  updatePost,
  removePost,
  getSearchPosts,
} = require("../controllers/post");

const {
  getRole,
  getRoles,
  createRole,
  removeRole,
  updateRole,
} = require("../controllers/role");

const {
  getSetting,
  getSettings,
  createSetting,
  removeSetting,
  updateSetting,
} = require("../controllers/setting");

router.get("/getsingleuser/:id", getUser);

router.get("/getuser", getUsers);

router.post("/createuser", createUser);

router.delete("/removeuser/:id", removeUser);

router.patch("/updateuser/:id", updateUser);

// post routes

router.get("/getsinglepost/:id", getPost);

router.get("/getpost", getPosts);

router.post("/createpost", createPost);

router.delete("/removepost/:id", removePost);

router.patch("/updatepost/:id", updatePost);

router.get("/search/:id", getSearchPosts);

//role routes

router.get("/getsinglerole/:id", getRole);

router.get("/getrole", getRoles);

router.post("/createrole", createRole);

router.delete("/removerole/:id", removeRole);

router.patch("/updaterole/:id", updateRole);

//settings

router.get("/getsinglesetting/:id", getSetting);

router.get("/getsetting", getSettings);

router.post("/createsetting", createSetting);

router.delete("/removesetting/:id", removeSetting);

router.patch("/updatesetting/:id", updateSetting);

module.exports = router;
