const express = require("express");

const router = express.Router();

const { signin, verifyToken } = require("../controllers/user");
const { auth } = require("../middlewares/auth");

router.post("/signin", signin);

router.post("/verifytoken", auth, verifyToken);

module.exports = router;
