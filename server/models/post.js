const mongoose = require("mongoose");

const postScheme = mongoose.Schema({
  title: {
    type: String,
  },
  summary: {
    type: String,
  },
  details: {
    type: String,
  },
  seoTitle: {
    type: String,
  },
  seoDescription: {
    type: String,
  },
  image: {
    type: String,
  },

  status: {
    type: Boolean,
  },
  isVerified: {
    type: Boolean,
  },
});

const Post = mongoose.model("Post", postScheme);

module.exports = Post;
