const mongoose = require("mongoose");

const settingSchema = mongoose.Schema({
  status: {
    type: Boolean,
  },
  title: {
    type: String,
  },
  uniqueName: {
    type: String,
  },
  description: {
    type: String,
  },
  settingValue: {
    type: [String],
  },
});

const Setting = mongoose.model("Setting", settingSchema);

module.exports = Setting;
