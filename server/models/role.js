const mongoose = require("mongoose");

const roleSchema = mongoose.Schema({
  title: {
    type: String,
  },
  slug: {
    type: String,
  },
  post: {
    type: [String],
  },
  user: {
    type: [String],
  },
  role: {
    type: [String],
  },
});

const Role = mongoose.model("Role", roleSchema);

module.exports = Role;
