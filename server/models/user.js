const mongoose = require("mongoose");

const userSchema = mongoose.Schema({
  email: {
    type: String,
  },
  password: {
    type: String,
  },
  name: {
    type: String,
  },

  role: {
    type: String,
  },
  gender: {
    type: String,
    enum: ["Male", "Female"],
  },
  isActive: {
    type: Boolean,
  },
  referral: {
    type: String,
  },
});

const User = mongoose.model("User", userSchema);

module.exports = User;
