const mongoose = require("mongoose");

const User = require("../models/user");

module.exports.getUsers = async (req, res) => {
  try {
    const users = await User.find();

    res.status(200).json(users);
  } catch (error) {
    console.log(error);
  }
};

module.exports.getUser = async (req, res) => {
  try {
    const user = await User.findById(req.params.id);
    res.status(200).json(user);
  } catch (error) {
    console.log(error);
  }
};

module.exports.createUser = async (req, res) => {
  try {
    const user = req.body;

    const exist = await User.findOne({ email: user.email });

    if (exist) return res.status(400).json({ message: "User already exist" });

    const newUser = await User.create(user);

    res.status(200).json({ newUser });
  } catch (error) {
    console.log(error);
  }
};

module.exports.removeUser = async (req, res) => {
  try {
    const { id } = req.params;

    if (!mongoose.Types.ObjectId.isValid(id))
      return res.satus(404).send("Can not found the post !");
    await User.findByIdAndRemove(id);

    res.status(200).json({ message: "User has been deleted successfully" });
  } catch (error) {
    console.log(error);
  }
};

module.exports.updateUser = async (req, res) => {
  try {
    const { id } = req.params;
    const user = req.body;

    if (!mongoose.Types.ObjectId.isValid(id))
      return res.satus(404).send("Can not found the user !");

    const updatedUser = await User.findByIdAndUpdate(
      id,
      { ...user, id },
      { new: true }
    );

    res.status(200).json(updatedUser);
  } catch (error) {
    console.log(error);
  }
};
