const mongoose = require("mongoose");

const Post = require("../models/post");

module.exports.getPosts = async (req, res) => {
  try {
    const posts = await Post.find();

    res.status(200).json(posts);
  } catch (error) {
    console.log(error);
  }
};

module.exports.getPost = async (req, res) => {
  try {
    if (!mongoose.Types.ObjectId.isValid(req.params.id))
      return res.status(404).send("Can not found the post !");

    const post = await Post.findById(req.params.id);
    res.status(200).json(post);
  } catch (error) {
    console.log(error);
  }
};

module.exports.createPost = async (req, res) => {
  try {
    const post = req.body;

    const newPost = await Post.create({
      ...post,
    });

    res.status(200).json({ newPost });
  } catch (error) {
    console.log(error);
  }
};

module.exports.removePost = async (req, res) => {
  try {
    const { id } = req.params;

    if (!mongoose.Types.ObjectId.isValid(id))
      return res.status(404).send("Can not found the post !");

    await Post.findByIdAndRemove(id);

    res.status(200).json({ message: "Post has been deleted successfully" });
  } catch (error) {
    console.log(error);
  }
};

module.exports.updatePost = async (req, res) => {
  try {
    const { id } = req.params;
    const post = req.body;

    if (!mongoose.Types.ObjectId.isValid(id))
      return res.status(404).send("Can not found the post !");

    const updatedPost = await Post.findByIdAndUpdate(
      id,
      { ...post, id },
      { new: true }
    );

    res.status(200).json(updatedPost);
  } catch (error) {
    console.log(error);
  }
};

module.exports.getSearchPosts = async (req, res) => {
  try {
    const posts = await Post.find(
      { $text: { $search: `\"${req.params.id}\"` } },
      { score: { $meta: "textScore" } }
    ).sort({ date: 1, score: { $meta: "textScore" } });

    res.status(200).json(posts);
  } catch (error) {
    console.log(error);
  }
};
