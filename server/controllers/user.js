const mongoose = require("mongoose");
const jwt = require("jsonwebtoken");

const jtw_decode = require("jwt-decode");

const User = require("../models/user");
const Role = require("../models/role");

const admin = {
  email: "admin@gmail.com",
  password: "admin",
};

module.exports.signin = async (req, res) => {
  try {
    const { email, password } = req.body;

    let token = jwt.sign(
      { email: email, role: "super-admin" },
      process.env.JWT_SECRET,
      {
        expiresIn: process.env.JWT_EXPIRES,
      }
    );

    if (email == admin.email && password == admin.password)
      return res.status(200).json(token);

    const user = await User.findOne({ email });

    if (!user)
      return res.status(404).json({ message: "Invalid email/password" });

    if (user.password == password) {
      token = jwt.sign(
        { email: user.email, role: user.role },
        process.env.JWT_SECRET,
        {
          expiresIn: process.env.JWT_EXPIRES,
        }
      );
      res.status(200).json(token);
    } else {
      return res.status(404).json({ message: "Invalid email/password" });
    }
  } catch (error) {
    console.log(error);
  }
};

module.exports.verifyToken = async (req, res) => {
  try {
    const token = req.body.userToken;

    const decode = jtw_decode(token);

    res.status(200).json(decode);
  } catch (error) {
    console.log(error);
  }
};
