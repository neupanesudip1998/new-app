const mongoose = require("mongoose");

const Role = require("../models/role");

module.exports.getRoles = async (req, res) => {
  try {
    const roles = await Role.find();

    res.status(200).json(roles);
  } catch (error) {
    console.log(error);
  }
};

module.exports.getRole = async (req, res) => {
  try {
    const role = await Role.findById(req.params.id);
    res.status(200).json(role);
  } catch (error) {
    console.log(error);
  }
};

module.exports.createRole = async (req, res) => {
  try {
    const role = req.body;

    const exist = await Role.findOne({ slug: role.slug });

    if (exist) return res.status(400).json({ message: "Role already exist" });

    const newRole = await Role.create(role);

    res.status(200).json({ newRole });
  } catch (error) {
    console.log(error);
  }
};

module.exports.removeRole = async (req, res) => {
  try {
    const { id } = req.params;

    if (!mongoose.Types.ObjectId.isValid(id))
      return res.satus(404).send("Can not found the role !");
    await Role.findByIdAndRemove(id);

    res.status(200).json({ message: "Role has been deleted successfully" });
  } catch (error) {
    console.log(error);
  }
};

module.exports.updateRole = async (req, res) => {
  try {
    const { id } = req.params;
    const role = req.body;

    if (!mongoose.Types.ObjectId.isValid(id))
      return res.satus(404).send("Can not found the role !");

    const updatedRole = await Role.findByIdAndUpdate(
      id,
      { ...role, id },
      { new: true }
    );

    res.status(200).json(updatedRole);
  } catch (error) {
    console.log(error);
  }
};
