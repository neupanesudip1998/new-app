const mongoose = require("mongoose");

const Setting = require("../models/setting");

module.exports.getSettings = async (req, res) => {
  try {
    const settings = await Setting.find();

    res.status(200).json(settings);
  } catch (error) {
    console.log(error);
  }
};

module.exports.getSetting = async (req, res) => {
  try {
    const setting = await Setting.findById(req.params.id);
    res.status(200).json(setting);
  } catch (error) {
    console.log(error);
  }
};

module.exports.createSetting = async (req, res) => {
  try {
    const setting = req.body;

    const exist = await Setting.findOne({ uniqueName: setting.uniqueName });

    if (exist)
      return res.status(400).json({ message: "setting already exist" });

    const newSetting = await Setting.create(setting);

    res.status(200).json({ newSetting });
  } catch (error) {
    console.log(error);
  }
};

module.exports.removeSetting = async (req, res) => {
  try {
    const { id } = req.params;

    if (!mongoose.Types.ObjectId.isValid(id))
      return res.satus(404).send("Can not found the setting !");
    await Setting.findByIdAndRemove(id);

    res.status(200).json({ message: "setting has been deleted successfully" });
  } catch (error) {
    console.log(error);
  }
};

module.exports.updateSetting = async (req, res) => {
  try {
    const { id } = req.params;
    const setting = req.body;

    if (!mongoose.Types.ObjectId.isValid(id))
      return res.satus(404).send("Can not found the setting !");

    const updatedSetting = await Setting.findByIdAndUpdate(
      id,
      { ...setting, id },
      { new: true }
    );

    res.status(200).json(updatedSetting);
  } catch (error) {
    console.log(error);
  }
};
