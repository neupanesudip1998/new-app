const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const bodyParser = require("body-parser");
const dotenv = require("dotenv");

const app = express();
dotenv.config();

app.use(bodyParser.json({ limit: "30mb", extended: true }));
app.use(bodyParser.urlencoded({ limit: "30mb", extended: true }));
app.use(cors({ origin: true, credentials: true }));

const adminRouter = require("./routes/admin");

const userRouter = require("./routes/user");

const { auth } = require("./middlewares/auth");

const { role } = require("./middlewares/role");

app.use("/admin", [auth, role], adminRouter);

app.use("/user", userRouter);

mongoose
  .connect(process.env.MONGO_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() =>
    app.listen(process.env.PORT, () =>
      console.log(`Server running at ${process.env.PORT}`)
    )
  )
  .catch((error) => {
    console.log(error);
  });
