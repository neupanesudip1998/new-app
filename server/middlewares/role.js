const jtw_decode = require("jwt-decode");

const Role = require("../models/role");
const User = require("../models/user");

module.exports.role = async (req, res, next) => {
  try {
    const token = req.headers.authorization?.split(" ")[1];
    if (token) {
      const decode = jtw_decode(token);

      const email = decode?.email;

      const user = await User.findOne({ email });

      if (user) {
        const role = await Role.findOne({ slug: user.role });

        const array = req.originalUrl.split("/");

        const url = array.filter(
          (a) => a.includes("role") || a.includes("user") || a.includes("post")
        )[0];

        if (!url) {
          return next();
        }

        const action = url.slice(-4);

        switch (action) {
          case "post":
            if (role.post.includes(url)) return next();
            break;
          case "user":
            if (role.user.includes(url)) return next();
            break;
          case "role":
            if (role.role.includes(url)) return next();
            break;

          default:
            break;
        }
      } else {
        res.status(401).json({ message: "User not found" });
      }
    } else {
      res.redirect(`http://localhost:${process.env.PORT}`);
    }
  } catch (error) {}
};
