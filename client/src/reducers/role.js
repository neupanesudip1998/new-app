export default (state = [], action) => {
  switch (action.type) {
    case "GET_ROLE":
      return { ...state, role: action.payload };

    case "GET_ROLES":
      return { ...state, roles: action.payload };

    case "CREATE_ROLE":
      return { roles: [...state.roles, action.payload] };

    case "UPDATE_ROLE":
      return {
        roles: state.roles.map((role) =>
          role._id === action.payload._id ? action.payload : role
        ),
      };

    case "DELETE_ROLE":
      return {
        roles: state.roles.filter((role) => role._id !== action.payload),
      };

    default:
      return state;
  }
};
