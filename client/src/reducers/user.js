export default (state = [], action) => {
  switch (action.type) {
    case "GET_USER":
      return { ...state, user: action.payload };

    case "GET":
      return { ...state, users: action.payload };

    case "CREATE":
      return { users: [...state.users, action.payload] };

    case "UPDATE":
      return {
        users: state.users.map((user) =>
          user._id === action.payload._id ? action.payload : user
        ),
      };

    case "DELETE":
      return {
        users: state.users.filter((user) => user._id !== action.payload),
      };

    default:
      return state;
  }
};
