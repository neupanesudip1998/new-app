import { combineReducers } from "redux";

import authReducer from "./auth";

import users from "./user";

import posts from "./posts";

import roles from "./role";

import settings from "./settings";

export default combineReducers({ authReducer, users, posts, roles, settings });
