export default (state = [], action) => {
  switch (action.type) {
    case "GET_POST":
      return { ...state, post: action.payload };

    case "GET_POSTS":
      return { ...state, posts: action.payload };

    case "SEARCH":
      return { ...state, posts: action.payload };
    case "CREATE_POST":
      return { posts: [...state.posts, action.payload] };

    case "UPDATE_POST":
      return {
        posts: state.posts.map((post) =>
          post._id === action.payload._id ? action.payload : post
        ),
      };

    case "DELETE_POST":
      return {
        posts: state.posts.filter((post) => post._id !== action.payload),
      };

    default:
      return state;
  }
};
