export default (state = [], action) => {
  switch (action.type) {
    case "GET_SETTING":
      return { ...state, setting: action.payload };

    case "GET_SETTINGS":
      return { ...state, settings: action.payload };

    case "CREATE_SETTING":
      return { settings: [...state.settings, action.payload] };

    case "UPDATE_SETTING":
      return {
        settings: state.settings.map((setting) =>
          setting._id === action.payload._id ? action.payload : setting
        ),
      };

    case "DELETE_SETTING":
      return {
        settings: state.settings.filter(
          (setting) => setting._id !== action.payload
        ),
      };

    default:
      return state;
  }
};
