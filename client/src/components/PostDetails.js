import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import { getPost } from "../actions/post";

const PostDetails = () => {
  const { id } = useParams();

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getPost(id));
  }, [id]);

  const { post } = useSelector((state) => state.posts);

  return (
    <>
      {post ? (
        <div>
          <h1> {post.title}</h1>
          <p>{post.summary}</p>

          <img
            style={{ maxWidth: "500px" }}
            src={post.image}
            alt={post.title}
          />

          <div dangerouslySetInnerHTML={{ __html: post.details }}></div>

          <p>{post.seoTitle}</p>

          <p>{post.seoDescription}</p>

          <p>
            {post.status ? <p>status verified</p> : <p>status unverified</p>}
          </p>
        </div>
      ) : (
        <h1>Page not found</h1>
      )}
    </>
  );
};

export default PostDetails;
