import React from "react";
import { useNavigate } from "react-router-dom";

const Post = ({ post }) => {
  const history = useNavigate();

  const newSummary = post.summary?.substring(0, 30);

  const handleClick = () => {
    history(`/postdetails/${post._id}`);
  };

  const style = {};

  return (
    <div className="post-card">
      <h1> {post.title}</h1>
      <h3>{`${newSummary} ....`}</h3>

      <img style={{ maxWidth: "100px" }} src={post.image} />

      <button onClick={handleClick}>view</button>
    </div>
  );
};

export default Post;
