import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { getPosts, getSearchPosts } from "../actions/post";

import Post from "./Post";

const Dashbord = ({ setUser }) => {
  const [query, setQuery] = useState("");

  const dispatch = useDispatch();
  const history = useNavigate();

  const handleClick = () => {
    dispatch({ type: "LOGOUT" });
    setUser(null);
    history("/");
  };

  useEffect(() => {
    dispatch(getPosts());
  }, [window.location.pathname]);

  const handleSearch = () => {
    if (query) dispatch(getSearchPosts(query));
  };

  const { posts } = useSelector((state) => state.posts);

  return (
    <div>
      Dashbord
      <button onClick={handleClick}>logout</button>
      <input
        type="text"
        name="search"
        label="search"
        placeholder="search"
        value={query}
        onChange={(e) => setQuery(e.target.value)}
        required
      />
      <button onClick={handleSearch}>search</button>
      {posts?.length < 1 ? (
        <h1 style={{ height: "20vh" }}>Sorry !! No Post Found </h1>
      ) : (
        <div className="posts-card">
          {posts?.map((post) => (
            <Post key={post._id} post={post} />
          ))}
        </div>
      )}
    </div>
  );
};

export default Dashbord;
