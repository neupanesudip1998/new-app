import React, { useState } from "react";
import { useDispatch } from "react-redux";

import { signin } from "../actions/auth";

const Home = () => {
  const initialState = {
    email: "",
    password: "",
  };

  const [form, setForm] = useState(initialState);

  const dispatch = useDispatch();

  const handleSubmit = (e) => {
    e.preventDefault();
    console.log(form);

    dispatch(signin(form));
  };
  return (
    <>
      <form onSubmit={handleSubmit}>
        <input
          type="email"
          name="email"
          label="email"
          placeholder="email"
          min={0}
          onChange={(e) => setForm({ ...form, email: e.target.value })}
          required
        />
        <br />

        <input
          type="password"
          name="password"
          label="password"
          placeholder="password"
          min={0}
          onChange={(e) => setForm({ ...form, password: e.target.value })}
          required
        />
        <br />

        <button type="submit">Submit</button>
      </form>
      ;
    </>
  );
};

export default Home;
