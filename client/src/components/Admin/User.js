import React from "react";

const User = ({ user, setId, setAction }) => {
  return (
    <div
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "space-between",
      }}
    >
      <h1>{user.name}</h1>
      <p>{user.email}</p>
      <button
        onClick={() => {
          setId(user._id);
          setAction("update");
        }}
      >
        update
      </button>
      <button
        onClick={() => {
          setId(user._id);
          setAction("delete");
        }}
      >
        delete
      </button>
    </div>
  );
};

export default User;
