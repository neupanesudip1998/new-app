import React from "react";
import { useNavigate } from "react-router-dom";

const Post = ({ post, setId, setAction }) => {
  const history = useNavigate();

  const newSummary = post?.summary.substring(0, 30);

  const handleClick = () => {
    history(`/postdetails/${post._id}`);
  };

  return (
    <div className="post-card">
      <h1> {post.title}</h1>
      <h3>{`${newSummary} ....`}</h3>

      <img style={{ maxWidth: "100px" }} src={post.image} />

      <button className="post-button" onClick={handleClick}>
        view
      </button>

      <button
        onClick={() => {
          setId(post._id);
          setAction("update");
        }}
      >
        update
      </button>
      <button
        onClick={() => {
          setId(post._id);
          setAction("delete");
        }}
      >
        delete
      </button>
    </div>
  );
};

export default Post;
