import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import slugify from "slugify";

import {
  getUser,
  getUsers,
  createUser,
  updateUser,
  removeUser,
} from "../../actions/admin";
import { getRoles } from "../../actions/role";
import { Url } from "./Url";
import User from "./User";

const Admin = () => {
  const initialState = {
    email: "",
    password: "",
    name: "",
    role: "",
    gender: "",
    isActive: false,
    referral: "",
  };

  const [form, setForm] = useState(initialState);

  const [id, setId] = useState(null);

  const [action, setAction] = useState(null);

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getUsers());
    dispatch(getRoles());
  }, [window.location.pathname]);

  const handleSubmit = (e) => {
    e.preventDefault();
    if (form.gender) {
      if (id && action == "update") {
        dispatch(updateUser(id, form));
        setAction("");
        setId("");
      } else {
        dispatch(createUser(form));
      }

      setForm(initialState);
    } else {
      console.log("fill all value");
    }
  };

  const { users } = useSelector((state) => state.users);
  const { roles } = useSelector((state) => state.roles);

  const handleUpdate = async () => {
    dispatch(getUser(id));
  };

  const handleDelete = async () => {
    dispatch(removeUser(id));
  };

  useEffect(() => {
    if (id && action == "update") handleUpdate();

    if (id && action == "delete") handleDelete();
  }, [id, action]);

  const { user } = useSelector((state) => state.users);

  useEffect(() => {
    if (user) setForm(user);
  }, [user]);

  return (
    <>
      <Url />

      <form onSubmit={handleSubmit}>
        <p>Create User</p>
        <p>Full name</p>
        <input
          type="text"
          name="name"
          label="name"
          placeholder="name"
          min={0}
          value={form?.name}
          onChange={(e) => setForm({ ...form, name: e.target.value })}
          required
        />

        <p>email</p>
        <input
          type="email"
          name="email"
          label="email"
          placeholder="email"
          min={0}
          value={form?.email}
          onChange={(e) =>
            setForm({
              ...form,
              email: slugify(e.target.value, { lower: true }),
            })
          }
          required
        />

        <p>password</p>

        <input
          type="password"
          name="password"
          label="password"
          placeholder="password"
          min={0}
          value={form?.password}
          onChange={(e) => setForm({ ...form, password: e.target.value })}
          required
        />

        <p>Role</p>

        <select
          required
          onChange={(e) => setForm({ ...form, role: e.target.value })}
        >
          <option value={form?.role}>{form?.role}</option>

          {roles?.length < 1 ? (
            <option disabled>no role found</option>
          ) : (
            roles?.map((role) => (
              <option key={role._id} value={role?.slug}>
                {role?.slug}
              </option>
            ))
          )}
        </select>

        <p>Gender</p>
        <div onChange={(e) => setForm({ ...form, gender: e.target.value })}>
          <>
            <input
              defaultChecked={user?.gender && user.gender == "Male"}
              type="radio"
              value="Male"
              name="gender"
            />
            Male
            <input
              defaultChecked={user?.gender && user.gender == "Female"}
              type="radio"
              value="Female"
              name="gender"
            />
            Female
          </>
        </div>

        <p>Active</p>
        <div onChange={(e) => setForm({ ...form, isActive: e.target.value })}>
          <>
            <input
              defaultChecked={user?.isActive}
              type="radio"
              value="true"
              name="active"
            />
            Active
            <input
              defaultChecked={!user?.isActive}
              type="radio"
              value="false"
              name="active"
            />
            Inactive
          </>
        </div>

        <p>Refferal user</p>
        <select
          onChange={(e) => setForm({ ...form, referral: e.target.value })}
        >
          <option value={form?.referral}>{form?.referral}</option>

          {users?.length < 1 ? (
            <option disabled>no user found</option>
          ) : (
            users?.map((user) => (
              <option key={user._id} value={user?.email}>
                {user?.email}
              </option>
            ))
          )}
        </select>

        <button type="submit">Submit</button>
      </form>
      <div>
        {users?.length < 1 ? (
          <h1 style={{ height: "20vh" }}>Sorry !! No User Found </h1>
        ) : (
          users?.map((user) => (
            <User
              key={user._id}
              user={user}
              setId={setId}
              setAction={setAction}
            />
          ))
        )}
      </div>
    </>
  );
};

export default Admin;
