import React, { useState, useEffect } from "react";
import FileBase from "react-file-base64";
import { useDispatch, useSelector } from "react-redux";
import { createPost, getPost, updatePost } from "../../actions/post";
import ReactSwitch from "react-switch";

import ReactQuill from "react-quill";
import "react-quill/dist/quill.snow.css";

const Form = ({ postId, setPostId }) => {
  const [convertedText, setConvertedText] = useState("");

  const [error, setError] = useState("");

  const [postData, setPostDate] = useState({
    title: "",
    summary: "",
    details: "",
    seoTitle: "",
    seoDescription: "",
    image: "",
    status: false,
    isVerified: false,
  });

  const dispatch = useDispatch();

  const { post } = useSelector((state) => state.posts);

  useEffect(() => {
    if (postId) {
      dispatch(getPost(postId));
    }
  }, [postId]);

  useEffect(() => {
    setPostDate(post);
    setConvertedText(post?.details);
  }, [post]);

  const handleSubmit = async (e) => {
    e.preventDefault();

    if (!postData.image || !convertedText) {
      setError("Fill all value");
    } else {
      if (postId) {
        dispatch(updatePost(postId, { ...postData, details: convertedText }));
      } else {
        dispatch(createPost({ ...postData, details: convertedText }));
      }
      reset();
      setPostId(null);
    }
  };

  const reset = () => {
    setPostDate({
      title: "",
      summary: "",
      details: "",
      seoTitle: "",
      seoDescription: "",
      image: "",
      status: false,
      isVerified: false,
    });
    setConvertedText("");
  };

  return (
    <div>
      <form onSubmit={handleSubmit} className="post-form">
        {error && (
          <div>
            <h1 onClick={() => setError("")}>&#10008;</h1>
            <p>{error}</p>
          </div>
        )}

        <p>title</p>
        <input
          type="text"
          name="title"
          label="title"
          placeholder="title"
          value={postData?.title}
          onChange={(e) => setPostDate({ ...postData, title: e.target.value })}
          required
        />
        <p>Summary</p>
        <textarea
          rows="4"
          cols="50"
          name="summary"
          label="summary"
          placeholder="summary"
          min={0}
          value={postData?.summary}
          onChange={(e) =>
            setPostDate({ ...postData, summary: e.target.value })
          }
          required
        />

        <p>Details</p>
        <ReactQuill
          theme="snow"
          value={convertedText}
          onChange={setConvertedText}
          style={{ maxWidth: "500px" }}
        />

        <p>seo title</p>
        <input
          type="text"
          name="seoTitle"
          label="seoTitle"
          placeholder="seoTitle"
          value={postData?.seoTitle}
          onChange={(e) =>
            setPostDate({
              ...postData,
              seoTitle: e.target.value,
            })
          }
          required
        />

        <p>seo discription</p>
        <textarea
          rows="4"
          cols="50"
          type="text"
          name="discription"
          label="discription"
          placeholder="discription"
          value={postData?.seoDescription}
          onChange={(e) =>
            setPostDate({ ...postData, seoDescription: e.target.value })
          }
          required
        />

        <p>status</p>
        <ReactSwitch
          checked={postData?.status}
          onChange={(e) => setPostDate({ ...postData, status: e })}
        />

        <p>is verified</p>
        <ReactSwitch
          checked={postData?.isVerified}
          onChange={(e) => setPostDate({ ...postData, isVerified: e })}
        />

        <p>images</p>

        {postData?.image && (
          <img
            style={{ maxWidth: "100px" }}
            className="image"
            src={postData.image}
          />
        )}

        <FileBase
          type="file"
          multiple={false}
          onDone={({ base64 }) => setPostDate({ ...postData, image: base64 })}
        />

        {/* <button className="btn-reset" onClick={reset}>
          Reset
        </button> */}
        <button className="btn-submit" type="submit">
          Submit{" "}
        </button>
      </form>
    </div>
  );
};

export default Form;
