import React from "react";

const Role = ({ role, setId, setAction }) => {
  return (
    <div
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "space-between",
      }}
    >
      <h1>{role.title}</h1>
      {/* <p>{role.email}</p> */}
      <button
        onClick={() => {
          setId(role._id);
          setAction("update");
        }}
      >
        update
      </button>
      <button
        onClick={() => {
          setId(role._id);
          setAction("delete");
        }}
      >
        delete
      </button>
    </div>
  );
};

export default Role;
