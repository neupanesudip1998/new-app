import React from "react";

const Setting = ({ setting, setId, setAction }) => {
  let array = [];
  for (let i = 0; i < setting.settingValue.length; i++) {
    array.push(<p>{setting.settingValue[i]}</p>);
    if (i === 5) break;
  }

  return (
    <div
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "space-between",
      }}
    >
      <h1>{setting.title}</h1>

      {array.map((s, i) => (
        <span key={i}>{s}</span>
      ))}

      <button
        onClick={() => {
          setId(setting._id);
          setAction("update");
        }}
      >
        update
      </button>
      <button
        onClick={() => {
          setId(setting._id);
          setAction("delete");
        }}
      >
        delete
      </button>
    </div>
  );
};

export default Setting;
