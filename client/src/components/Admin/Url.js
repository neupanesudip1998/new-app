import React from "react";
import { Link } from "react-router-dom";

export const Url = () => {
  return (
    <div>
      <Link to="/home">Home</Link>
      <Link to="/admin/role">Roles</Link>
      <Link to="/admin/posts">Posts</Link>
      <Link to="/admin/posts/post">Create Post</Link>
      <Link to="/admin/users">Users</Link>
      <Link to="/admin/settings">Settings</Link>
    </div>
  );
};
