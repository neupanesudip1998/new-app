import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import slugify from "slugify";
import { Url } from "./Url";

import {
  getRole,
  getRoles,
  createRole,
  updateRole,
  removeRole,
} from "../../actions/role";

import Role from "./Role";

const Roles = () => {
  const dispatch = useDispatch();

  const initialState = {
    title: "",
    slug: "",
    post: [],
    user: [],
    role: [],
  };

  const fields = ["post", "user", "role"];

  const actions = ["Get", "Get Single", "Create", "Update", "Remove"];

  const [form, setForm] = useState(initialState);

  const [id, setId] = useState(null);

  const [action, setAction] = useState(null);

  const handleSubmit = (e) => {
    e.preventDefault();
    if (id && action == "update") {
      dispatch(updateRole(id, form));
      setAction("");
      setId("");
    } else {
      dispatch(createRole(form));
    }
    setForm(initialState);
  };

  useEffect(() => {
    dispatch(getRoles());
  }, [window.location.pathname]);

  const { roles } = useSelector((state) => state.roles);

  const handleUpdate = async () => {
    dispatch(getRole(id));
  };

  const handleDelete = async () => {
    dispatch(removeRole(id));
  };

  useEffect(() => {
    if (id && action == "update") handleUpdate();

    if (id && action == "delete") handleDelete();
  }, [id, action]);

  const { role } = useSelector((state) => state.roles);

  useEffect(() => {
    if (role) setForm(role);
  }, [role]);

  const handleChange = (e, field) => {
    const { value, checked } = e.target;

    const slugValue = slugify(value, {
      replacement: "",
      lower: true,
    });

    const { post, user, role } = form;

    switch (field.f) {
      case "post":
        if (checked) {
          setForm({ ...form, post: [...post, slugValue] });
        } else {
          setForm({
            ...form,
            post: post.filter((e) => e !== slugValue),
          });
        }
        break;
      case "user":
        if (checked) {
          setForm({ ...form, user: [...user, slugValue] });
        } else {
          setForm({
            ...form,
            user: user.filter((e) => e !== slugValue),
          });
        }
        break;
      case "role":
        if (checked) {
          setForm({ ...form, role: [...role, slugValue] });
        } else {
          setForm({
            ...form,
            role: role.filter((e) => e !== slugValue),
          });
        }
        break;
      default:
        break;
    }
  };

  const check = (i, value) => {
    const slugValue = slugify(value, {
      replacement: "",
      lower: true,
    });

    if (i == 0) return form.post.includes(slugValue);
    if (i == 1) return form.user.includes(slugValue);
    if (i == 2) return form.role.includes(slugValue);
  };

  const handleSlug = (e) => {
    const { value } = e.target;
    const slug = slugify(value, { replacement: "", lower: true, strict: true });
    setForm({ ...form, title: value, slug: slug });
  };

  return (
    <>
      <Url />

      <form onSubmit={handleSubmit}>
        <p>Add role</p>
        <p>Title</p>
        <input
          type="text"
          name="title"
          label="title"
          placeholder="title"
          min={0}
          value={form?.title}
          onChange={(e) => handleSlug(e)}
          required
        />
        <br />

        <p>Slug</p>

        <input
          type="text"
          name="slug"
          label="slug"
          placeholder="slug"
          min={0}
          value={form?.slug}
        />
        <br />

        {fields.map((f, i) => {
          return (
            <div key={i}>
              <p>{f.toUpperCase()}</p>
              {actions.map((a, j) => {
                return (
                  <>
                    <input
                      key={j}
                      type="checkbox"
                      name={f}
                      value={`${a} ${f}`}
                      checked={check(i, `${a} ${f}`)}
                      onChange={(e) => handleChange(e, { f })}
                    />
                    {`${a} ${f}`}
                  </>
                );
              })}
              <br />
              <br />
            </div>
          );
        })}

        <br />

        <button type="submit">Submit</button>
      </form>
      <div>
        {roles?.length < 1 ? (
          <h1 style={{ height: "20vh" }}>Sorry !! No Role Found </h1>
        ) : (
          roles?.map((role) => (
            <Role
              key={role._id}
              role={role}
              setId={setId}
              setAction={setAction}
            />
          ))
        )}
      </div>
    </>
  );
};

export default Roles;
