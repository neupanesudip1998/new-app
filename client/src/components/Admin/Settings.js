import React, { useEffect, useState } from "react";
import ReactSwitch from "react-switch";
import slugify from "slugify";

import { Chip, FormControl, Input } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";

import Setting from "./Setting";

import {
  getSettings,
  createSetting,
  updateSetting,
  getSetting,
  removeSetting,
} from "../../actions/settings";

const Settings = () => {
  const initialState = {
    status: false,
    title: "",
    uniqueName: "",
    description: "",
    settingValue: [],
  };

  const dispatch = useDispatch();

  const [currValue, setCurrValue] = useState("");
  const [values, setValues] = useState([]);

  const [id, setId] = useState(null);

  const [action, setAction] = useState(null);

  const [form, setForm] = useState(initialState);

  useEffect(() => {
    dispatch(getSettings());
  }, [window.location.pathname]);

  const { settings } = useSelector((state) => state.settings);

  const handleSlug = (e) => {
    const { value } = e.target;
    const slug = slugify(value, {
      replacement: "-",
      lower: true,
      strict: true,
    });
    setForm({ ...form, title: value, uniqueName: slug });
  };

  const handleKeyUp = (e) => {
    if (e.keyCode == 32) {
      setValues((oldState) => [...oldState, e.target.value]);
      setCurrValue("");
    }
  };

  const handleChange = (e) => {
    setCurrValue(e.target.value);
  };

  const handleDelete = (item, index) => {
    let arr = [...values];
    arr.splice(index, 1);
    setValues(arr);
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    if (id && action == "update") {
      dispatch(updateSetting(id, { ...form, settingValue: [...values] }));
      setAction("");
      setId("");
    } else {
      dispatch(createSetting({ ...form, settingValue: [...values] }));
    }
    setForm(initialState);
    setValues([]);
  };

  const handleUpdate = async () => {
    dispatch(getSetting(id));
  };

  const handleRemove = async () => {
    dispatch(removeSetting(id));
  };

  useEffect(() => {
    if (id && action == "update") handleUpdate();

    if (id && action == "delete") handleRemove();
  }, [id, action]);

  const { setting } = useSelector((state) => state.settings);

  useEffect(() => {
    if (setting) {
      setForm(setting);
      setValues(setting.settingValue);
    }
  }, [setting]);

  return (
    <>
      <form onSubmit={handleSubmit}>
        <p>Settings's information</p>
        <p>Status</p>

        <ReactSwitch
          checked={form?.status}
          onChange={(e) => setForm({ ...form, status: e })}
        />

        <p>Title *</p>
        <input
          required
          type="text"
          name="title"
          label="title"
          placeholder="title"
          value={form?.title}
          onChange={(e) => handleSlug(e)}
        />

        <p>Unique Name *</p>
        <input
          type="text"
          name="slug"
          label="slug"
          placeholder="slug"
          min={0}
          value={form?.uniqueName}
        />

        <p>Description *</p>
        <input
          required
          type="text"
          name="description"
          label="description"
          placeholder="description"
          value={form?.description}
          onChange={(e) => setForm({ ...form, description: e.target.value })}
        />

        <p>Settings's Value *</p>
        <FormControl>
          <div>
            {values.map((item, index) => (
              <Chip
                size="small"
                onDelete={() => handleDelete(item, index)}
                label={item}
              />
            ))}
          </div>
          <Input
            value={currValue}
            onChange={handleChange}
            onKeyDown={handleKeyUp}
          />
        </FormControl>
        <br />
        <br />

        <button type="submit">submit</button>
      </form>

      <div>
        {settings?.length < 1 ? (
          <h1 style={{ height: "20vh" }}>Sorry !! No setting Found </h1>
        ) : (
          settings?.map((setting) => (
            <Setting
              key={setting._id}
              setting={setting}
              setId={setId}
              setAction={setAction}
            />
          ))
        )}
      </div>
    </>
  );
};

export default Settings;
