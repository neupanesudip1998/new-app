import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, useNavigate } from "react-router-dom";

import { getPosts, removePost } from "../../actions/post";

import Post from "./Post";
import { Url } from "./Url";

const Posts = ({ setPostId }) => {
  const history = useNavigate();

  const [id, setId] = useState(null);

  const [action, setAction] = useState(null);

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getPosts());
  }, [window.location.pathname]);

  const { posts } = useSelector((state) => state.posts);

  const handleDelete = async () => {
    dispatch(removePost(id));
  };

  useEffect(() => {
    if (id && action == "update") {
      setPostId(id);
      history("/admin/posts/post");
    }

    if (id && action == "delete") handleDelete();
  }, [id, action]);

  return (
    <>
      <Url />

      {posts?.length < 1 ? (
        <h1 style={{ height: "20vh" }}>Sorry !! No Post Found </h1>
      ) : (
        <div className="posts-card">
          {posts?.map((post) => (
            <Post
              key={post._id}
              post={post}
              setId={setId}
              setAction={setAction}
            />
          ))}
        </div>
      )}
    </>
  );
};

export default Posts;
