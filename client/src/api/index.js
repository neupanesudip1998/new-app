import axios from "axios";

const API = axios.create({ baseURL: "http://localhost:5000" });

const token = JSON.parse(localStorage.getItem("token"));

API.interceptors.request.use((req) => {
  if (localStorage.getItem("token")) {
    req.headers.Authorization = `Bearer ${token}`;
  }

  return req;
});

export const verifyToken = (token) => API.post("/user/verifytoken", token);

export const signin = (data) => API.post("/user/signin", data);

export const getUser = (userId) => API.get(`/admin/getsingleuser/${userId}`);

export const getUsers = () => API.get("/admin/getuser");

export const createUser = (data) => API.post("/admin/createuser", data);

export const updateUser = (id, data) =>
  API.patch(`/admin/updateuser/${id}`, data);

export const removeUser = (id) => API.delete(`/admin/removeuser/${id}`);

//post

export const getPost = (id) => API.get(`/admin/getsinglepost/${id}`);

export const getPosts = () => API.get("/admin/getpost");

export const createPost = (data) => API.post("/admin/createpost", data);

export const updatePost = (id, data) =>
  API.patch(`/admin/updatepost/${id}`, data);

export const removePost = (id) => API.delete(`/admin/removepost/${id}`);

export const getSearchPosts = (query) => API.get(`/admin/search/${query}`);

//role

export const getRole = (id) => API.get(`/admin/getsinglerole/${id}`);

export const getRoles = () => API.get("/admin/getrole");

export const createRole = (role) => API.post("/admin/createrole", role);

export const updateRole = (id, role) =>
  API.patch(`/admin/updaterole/${id}`, role);

export const removeRole = (id) => API.delete(`/admin/removerole/${id}`);

//settings

export const getSetting = (id) => API.get(`/admin/getsinglesetting/${id}`);

export const getSettings = () => API.get("/admin/getsetting");

export const createSetting = (setting) =>
  API.post("/admin/createsetting", setting);

export const updateSetting = (id, setting) =>
  API.patch(`/admin/updatesetting/${id}`, setting);

export const removeSetting = (id) => API.delete(`/admin/removesetting/${id}`);
