import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom";

import Home from "./components/Home";
import Admin from "./components/Admin/Admin";
import Dashbord from "./components/Dashbord";

import Posts from "./components/Admin/Posts";

import { verifyToken } from "./actions/auth";
import Form from "./components/Admin/Form";
import PostDetails from "./components/PostDetails";
import Roles from "./components/Admin/Roles";
import Settings from "./components/Admin/Settings";

function App() {
  const userToken = JSON.parse(localStorage.getItem("token"));

  const dispatch = useDispatch();

  const [user, setUser] = useState(null);

  const [postId, setPostId] = useState(null);

  const check = async () => {
    const data = await dispatch(verifyToken({ userToken }));
    if (data == "Token expires") {
      setUser(null);
    } else {
      setUser(data);
    }
  };

  useEffect(() => {
    if (userToken) {
      check();
    }
  }, [userToken]);

  return (
    <BrowserRouter>
      <Routes>
        {user ? (
          <>
            <Route path="/" exact element={<Navigate to="/home" />} />
            <Route
              path="/admin/users"
              exact
              element={user.role === "admin" ? <Admin /> : <Navigate to="/" />}
            />
            <Route
              path="/admin/posts"
              exact
              element={
                user.role === "admin" ? (
                  <Posts setPostId={setPostId} />
                ) : (
                  <Navigate to="/" />
                )
              }
            />

            <Route
              path="/admin/role"
              exact
              element={user.role === "admin" ? <Roles /> : <Navigate to="/" />}
            />

            <Route
              path="/admin/posts/post"
              exact
              element={
                user.role === "admin" ? (
                  <Form postId={postId} setPostId={setPostId} />
                ) : (
                  <Navigate to="/" />
                )
              }
            />

            <Route
              path="/home"
              exact
              element={<Dashbord setUser={setUser} />}
            />

            <Route
              path="/admin/settings"
              exact
              element={
                user.role === "admin" ? <Settings /> : <Navigate to="/" />
              }
            />

            <Route path="/postdetails/:id" exact element={<PostDetails />} />
          </>
        ) : (
          <>
            <Route path="/" exact element={<Home />} />

            <Route path="/*" exact element={<Home />} />
          </>
        )}
      </Routes>
    </BrowserRouter>
  );
}

export default App;
