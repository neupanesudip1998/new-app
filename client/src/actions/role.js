import * as api from "../api/index";

export const getRole = (id) => async (dispatch) => {
  try {
    const { data } = await api.getRole(id);
    dispatch({ type: "GET_ROLE", payload: data });
  } catch (error) {
    console.log(error);
  }
};

export const getRoles = () => async (dispatch) => {
  try {
    const { data } = await api.getRoles();
    dispatch({ type: "GET_ROLES", payload: data });
  } catch (error) {
    console.log(error);
  }
};

export const createRole = (role) => async (dispatch) => {
  try {
    const { data } = await api.createRole(role);
    dispatch({ type: "CREATE_ROLE", payload: data.newRole });
  } catch (error) {
    console.log(error);
  }
};

export const removeRole = (id) => async (dispatch) => {
  try {
    const { data } = await api.removeRole(id);

    dispatch({ type: "DELETE_ROLE", payload: id });
  } catch (error) {
    console.log(error);
  }
};

export const updateRole = (id, role) => async (dispatch) => {
  try {
    const { data } = await api.updateRole(id, role);

    dispatch({ type: "UPDATE_ROLE", payload: data });
  } catch (error) {
    console.log(error);
  }
};
