import * as api from "../api/index";

export const getSetting = (id) => async (dispatch) => {
  try {
    const { data } = await api.getSetting(id);
    dispatch({ type: "GET_SETTING", payload: data });
  } catch (error) {
    console.log(error);
  }
};

export const getSettings = () => async (dispatch) => {
  try {
    const { data } = await api.getSettings();
    dispatch({ type: "GET_SETTINGS", payload: data });
  } catch (error) {
    console.log(error);
  }
};

export const createSetting = (setting) => async (dispatch) => {
  try {
    console.log(setting);
    const { data } = await api.createSetting(setting);
    dispatch({ type: "CREATE_SETTING", payload: data.newSetting });
  } catch (error) {
    console.log(error);
  }
};

export const removeSetting = (id) => async (dispatch) => {
  try {
    const { data } = await api.removeSetting(id);

    dispatch({ type: "DELETE_SETTING", payload: id });
  } catch (error) {
    console.log(error);
  }
};

export const updateSetting = (id, role) => async (dispatch) => {
  try {
    const { data } = await api.updateSetting(id, role);

    dispatch({ type: "UPDATE_SETTING", payload: data });
  } catch (error) {
    console.log(error);
  }
};
