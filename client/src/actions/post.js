import * as api from "../api";

export const getPosts = () => async (dispatch) => {
  try {
    const { data } = await api.getPosts();
    dispatch({ type: "GET_POSTS", payload: data });
  } catch (error) {
    if (error.response.data.message == "Token expires") {
      dispatch({ type: "LOGOUT" });
    }
  }
};

export const getPost = (id) => async (dispatch) => {
  try {
    const { data } = await api.getPost(id);
    dispatch({ type: "GET_POST", payload: data });
  } catch (error) {
    console.log(error.response?.data.message);
  }
};

export const createPost = (post) => async (dispatch) => {
  try {
    const { data } = await api.createPost(post);
    dispatch({ type: "CREATE_POST", payload: data });
  } catch (error) {
    console.log(error.response);
  }
};

export const updatePost = (id, post) => async (dispatch) => {
  try {
    const { data } = await api.updatePost(id, post);
    dispatch({ type: "UPDATE_POST", payload: data });
  } catch (error) {
    console.log(error.response?.data.message);
  }
};

export const removePost = (id) => async (dispatch) => {
  try {
    await api.removePost(id);
    dispatch({ type: "DELETE_POST", payload: id });
  } catch (error) {
    console.log(error.response?.data.message);
  }
};

export const getSearchPosts = (query) => async (dispatch) => {
  try {
    const { data } = await api.getSearchPosts(query);
    dispatch({ type: "SEARCH", payload: data });
  } catch (error) {
    console.log(error);
  }
};
