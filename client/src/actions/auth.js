import * as api from "../api/index";

export const signin = (formData, history) => async (dispatch) => {
  try {
    const { data } = await api.signin(formData);
    dispatch({ type: "LOGIN", data });
    window.location.reload();
    history("/");
  } catch (error) {
    console.log(error);
  }
};

export const verifyToken = (token) => async (dispatch) => {
  try {
    const { data } = await api.verifyToken(token);
    return data;
  } catch (error) {
    return error.response.data.message;
  }
};
